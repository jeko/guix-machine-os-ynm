;; This is an operating system configuration template
;; for a "bare bones" setup, with no X11 display server.
(define-module (bare-bones))

(use-modules (gnu))
(use-service-modules networking ssh)
(use-package-modules screen ssh)

(define-public %bare-bones
  (operating-system
   (host-name "kitchen")
   (timezone "Europe/Paris")
   (locale "fr_FR.utf8")

   ;; Boot in "legacy" BIOS mode, assuming /dev/sdX is the
   ;; target hard disk, and "my-root" is the label of the target
   ;; root file system.
   (bootloader (bootloader-configuration
		(bootloader grub-bootloader)
		(target "/dev/vda")))
   (file-systems (cons (file-system
			(device "/dev/vda1")
			(mount-point "/")
			(type "ext4"))
                       %base-file-systems))

   ;; Globally-installed packages.
   (packages (cons screen %base-packages))

      ;; Add services to the baseline: a DHCP client and
   ;; an SSH server.
   (services
    (append
     (list
      (service dhcp-client-service-type)
      (service openssh-service-type
               (openssh-configuration
		(openssh openssh-sans-x)
		(permit-root-login 'without-password)
		(authorized-keys
		 `(("root" ,(local-file "id_rsa.pub"))))
		(port-number 22))))
     (modify-services
      %base-services
      (guix-service-type
       config => (guix-configuration
		  (inherit config)
		  (authorized-keys
		   (append
		    (list (local-file "/etc/guix/signing-key.pub"))
		    %default-authorized-guix-keys)))))))))

%bare-bones
