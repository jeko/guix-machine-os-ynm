(define-module (ynm-system-declaration))

(use-modules (gnu)
	     (gnu services))

(use-service-modules ssh networking web certbot)
(use-package-modules screen ssh certs version-control)

(define (project-cert-path file)
  (string-append "/etc/letsencrypt/live/yournextmeal.tech/" (symbol->string file) ".pem"))

(define-public %ynm-system-declaration
  (operating-system
   (host-name "kitchen")
   (timezone "Europe/Paris")
   (locale "fr_FR.utf8")
   (packages (cons* screen git nss-certs %base-packages))
   
   (bootloader (bootloader-configuration
		(bootloader
		 (bootloader
                  (inherit grub-bootloader)
                  (installer #~(const #true))))))
   
   (file-systems (cons (file-system
			(device "/dev/sda")
			(mount-point "/")
			(type "ext4"))
                       %base-file-systems))

   (swap-devices (list "/dev/sdb"))

   (initrd-modules (cons "virtio_scsi"
			 %base-initrd-modules))
   
   (services
    (append
     (list      
      (service dhcp-client-service-type)
      
      (service openssh-service-type
	       (openssh-configuration
		(openssh openssh-sans-x)
		(permit-root-login 'prohibit-password)
		(authorized-keys
		 `(("root" ,(local-file "id_ed25519_linode.pub"))))
		(port-number 2222)))

      (service certbot-service-type
               (certbot-configuration
		(email (string-append "chief@yournextmeal.tech"))
		(certificates
		 (list
		  (certificate-configuration
		   (domains (list "yournextmeal.tech"))
		   (deploy-hook
		    (program-file
		     "nginx-deploy-hook"
		     #~(let ((pid (call-with-input-file "/var/run/nginx/pid" read)))
			 (kill pid SIGHUP)))))))))
      
      (service nginx-service-type
               (nginx-configuration
                (server-blocks
                 (list (nginx-server-configuration
			(server-name (list "yournextmeal.tech"))
			(ssl-certificate (project-cert-path 'fullchain))
			(ssl-certificate-key (project-cert-path 'privkey))
			(locations
			 (list
			  (nginx-location-configuration ;certbot
                           (uri "/.well-known")
			   (body '("allow all;"
                                   "root /var/www;")))
			  (nginx-location-configuration ;artanis
			   (uri "/")
			   (body '("proxy_pass http://127.0.0.1:3000;"
				   "proxy_set_header Host $host;"
				   "proxy_set_header X-Real-IP $remote_addr;"
				   "proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;")))))))))))

     (modify-services %base-services
		      (guix-service-type ynm-config =>
					 (guix-configuration (inherit ynm-config)
							     (authorized-keys
							      (append
							       (list (local-file "/etc/guix/signing-key.pub"))
							       %default-authorized-guix-keys)))))))))

%ynm-system-declaration
