;; This is a Guix deployment of a "bare bones" setup, with
;; no X11 display server, to a machine with an SSH daemon
;; listening on localhost:2222. A configuration such as this
;; may be appropriate for virtual machine with ports
;; forwarded to the host's loopback interface.

(add-to-load-path (dirname (current-filename)))

(use-modules (bare-bones))

(define %system
  (operating-system
   (inherit %bare-bones)
   (host-name "kitchen-vm")))

(list (machine
       (operating-system %system)
       (environment managed-host-environment-type)
       (configuration (machine-ssh-configuration
                       (host-name "localhost")
                       (system "x86_64-linux")
                       (user "root")
                       (identity "id_rsa")
		       (host-key "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIDLfWsrykqFUbMS1vfqHJ+sduRqnMpcRKhs96VzOuz4a")
                       (port 10022)))))
