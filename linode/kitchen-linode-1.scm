(define-module (ynm-system-declaration-pre-letsencrypt))

(use-modules (gnu)
	     (gnu services))

(use-service-modules ssh networking web certbot)
(use-package-modules screen ssh certs version-control)

(define (project-cert-path file)
  (string-append "/etc/letsencrypt/live/yournextmeal.tech/" (symbol->string file) ".pem"))

(define-public %ynm-system-declaration-pre-letsencrypt
  (operating-system
   (host-name "kitchen")
   (timezone "Europe/Paris")
   (locale "fr_FR.utf8")
   (packages (cons* screen git nss-certs %base-packages))
   
   (bootloader (bootloader-configuration
		(bootloader
		 (bootloader
                  (inherit grub-bootloader)
                  (installer #~(const #true))))))
   
   (file-systems (cons (file-system
			(device "/dev/sda")
			(mount-point "/")
			(type "ext4"))
                       %base-file-systems))

   (swap-devices (list "/dev/sdb"))

   (initrd-modules (cons "virtio_scsi"
			 %base-initrd-modules))
   
   (services
    (append
     (list      
      (service dhcp-client-service-type)
      
      (service openssh-service-type
	       (openssh-configuration
		(openssh openssh-sans-x)
		(permit-root-login 'prohibit-password)
		(authorized-keys
		 `(("root" ,(local-file "id_ed25519_linode.pub"))))
		(port-number 2222)))

      (service certbot-service-type
               (certbot-configuration
		(email (string-append "chief@yournextmeal.tech"))
		(certificates
		 (list
		  (certificate-configuration
		   (domains (list "yournextmeal.tech"))
		   (deploy-hook
		    (program-file
		     "nginx-deploy-hook"
		     #~(let ((pid (call-with-input-file "/var/run/nginx/pid" read)))
			 (kill pid SIGHUP)))))))))
      
      (service nginx-service-type))

     (modify-services %base-services
		      (guix-service-type ynm-config =>
					 (guix-configuration (inherit ynm-config)
							     (authorized-keys
							      (append
							       (list (local-file "/etc/guix/signing-key.pub"))
							       %default-authorized-guix-keys)))))))))

%ynm-system-declaration-pre-letsencrypt
