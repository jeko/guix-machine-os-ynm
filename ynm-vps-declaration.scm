(add-to-load-path (getcwd))

(use-modules (gnu)
	     (ynm-system-declaration))

(list
 (machine
  (operating-system %ynm-system-declaration)
  (environment managed-host-environment-type)
  (configuration (machine-ssh-configuration
                  (host-name "yournextmeal.tech")
		  (host-key "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAICnI0HQg5GEb++NPIzPK2St5vR0fgjfYDLhJEerixhSn")
		  (system "x86_64-linux")
                  (user "root")
                  (identity "/home/jeko/.ssh/id_rsa.pub")
                  (port 2222)))))

