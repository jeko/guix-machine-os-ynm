(use-modules (gnu))

(use-service-modules networking ssh)
(use-package-modules admin tls screen ssh certs)

(operating-system
 (host-name "kitchen")
 (timezone "Europe/Paris")
 (locale "fr_FR.utf8")

 (packages (cons* screen nss-certs openssh-sans-x %base-packages))

 (bootloader (bootloader-configuration
              (bootloader
               (bootloader
                (inherit grub-bootloader)
                (installer #~(const #true))))))

 (file-systems (cons (file-system
                      (device "/dev/sda")
                      (mount-point "/")
                      (type "ext4"))
                     %base-file-systems))

 (swap-devices (list "/dev/sdb"))

 (initrd-modules (cons "virtio_scsi"    ; Needed to find the disk
                       %base-initrd-modules))

 (services
  (append
   (list
    (service dhcp-client-service-type)

    (service openssh-service-type
	     (openssh-configuration
	      (openssh openssh-sans-x)
	      (permit-root-login 'prohibit-password)
	      (authorized-keys
	       `(("root" ,(local-file "id_ed25519_linode.pub"))))
	      (port-number 2222))))

   %base-services)))
